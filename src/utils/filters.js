import Vue from 'vue'

//金额
Vue.filter('money', (value, digits = 2) => {
    let val = Number(value)
    if (!Number.isNaN(val)) {
        return val.toFixed(digits)
    }
    return val
})