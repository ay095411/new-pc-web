### 技术栈
- Vue-cli2.0
- Element-ui
- Less
- Router
- Axios
- Vuex

### 实现了哪些？
1. 项目加载svg动画
2. 路由封装(动态标题)
3. 开发/生产环境打包区分
4. Axios的封装，api文件抽离使其更好维护
5. Vuex封装
6. 过滤器/工具方法封装
7. Less全局变量配置


## 安装依赖包
```
npm install
```

### 项目启动
```
npm run serve
```

### 项目打包 / 测试环境
```
npm run build:stage
```

### 项目打包 / 生产环境
```
npm run build:prod
```