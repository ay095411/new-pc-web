import request from '@/utils/request'

// 登录方法
export function login(userInfo) {
	return request({
		url: '/loginByJson?JSESSIONID=' + userInfo.uuid,
		method: 'post',
		data: userInfo // params: query
	})
}

// 获取用户详细信息
export function getInfo(token) {
	return request({
		url: '/getInfo',
		method: 'get'
	})
}

// 退出方法
export function logout(data) {
	return request({
		url: '/staff/user/logout',
		method: 'post',
		data: data
	})
}

// 获取图形验证码
export function getCodeImg(JSESSIONID) {
	return request({
		url: '/captcha/captchaImageJson',
		method: 'get'
	})
}

// 获取短信验证码
export function getCodeMsg(uuid,phone) {
	return request({
		url: '/captcha/captchaSms?phone=' + phone + '&JSESSIONID=' + uuid,
		method: 'get'
	})
}

// 验证码校验
export function codeImgjudege(uuid,code) {
	return request({
		url: '/staff/user/info/captchajudege?code=' + code +'&JSESSIONID=' + uuid,
		method: 'get'
	})
}

// 重置密码
export function resetPassword(uuid,data) {
	return request({
		url: '/staff/user/info/resetPassword?JSESSIONID=' + uuid,
		method: 'post',
		data: data
	})
}