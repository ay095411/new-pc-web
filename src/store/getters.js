const getters = {
  token: state => state.user.token,
  name: state => state.user.name,
  baseApi: state => state.api.baseApi
}
export default getters
