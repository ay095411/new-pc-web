export default [{
	path: '/',
	name: 'home',
	meta: {
		title: '首页'
	},
	component: resolve => require(['../views/home.vue'], resolve)
}]
