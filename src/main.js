import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'
import VueRouter from 'vue-router'
import routes from './routes'
import utils from './utils/utils.js'
import './utils/filters.js' //引入过滤器

Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(ElementUI);

console.log(process.env)
const router = new VueRouter({
    mode: process.env.NODE_ENV === 'production' ? 'history' : 'hash',
    base: '',	// '/m',
    routes: routes
})

// 页面打开前
router.beforeEach(async (to, from, next) => {
    //设置标题
    if (to.meta && to.meta.title) {
        utils.setDocumentTitle(to.meta.title)
    }
	next()
})

//页面打开以后
router.afterEach((to, from) => {

})


new Vue({
    el: '#app',
    router,
    render: h => h(App),
    mounted: () => {
        Vue.nextTick(() => {
            document.body.removeChild(document.querySelector('.xx-svg__global'))
        })
    }
})