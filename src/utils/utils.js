export default {
    setDocumentTitle: title => {
        document.title = title;
    },
    random_string(len) {
        len = len || 32;
        let chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
        let maxPos = chars.length;
        let pwd = '';
        for (let i = 0; i < len; i++) {
            pwd += chars.charAt(Math.floor(Math.random() * maxPos));
        }
        return pwd;
    },
    get_suffix(filename) {
        let pos = filename.lastIndexOf('.')
        let suffix = ''
        if (pos != -1) {
            suffix = filename.substring(pos)
        }
        return suffix;
    },
    get_timestr() {
        let date = new Date()
        return [date.getUTCFullYear(), date.getUTCMonth() + 1, date.getUTCDate()].join('')
    },
    // 手机号码验证
    phone(phone) {
        if (/^[1][2,3,4,5,6,7,8,9][0-9]{9}$/.test(phone)) {
            return true
        }
        return false
    },
    // 截取url里面的属性值
    getParam(){
        var qs = location.search.substr(1),
          args = {},
          items = qs.length ? qs.split("&") : [],
          item = null,
          len = items.length;
        for(var i = 0; i < len; i++) {
          item = items[i].split("=");
          var name = decodeURIComponent(item[0]),
          value = decodeURIComponent(item[1]);
          if(name) {
            args[name] = value;
          }
        }
        return args;
    }
}
